//
//  SurfingSpotViewModelTests.swift
//  SurfingSpotsTests
//
//  Created by Paolo Piccini on 30/07/2020.
//  Copyright © 2020 Paolo Piccini. All rights reserved.
//

import XCTest
import Combine
@testable import SurfingSpots

class SurfingSpotViewModelTests: XCTestCase {
    
    var sut: SurfingSpotsViewModel!
    var mock: URLSessionMock!
    var subscriptions = Set<AnyCancellable>()
    let networkLayer = NetworkLayer()
    var cleanCityList = [
        City(name: "City001", image: "p0", temp: 33),
        City(name: "City002", image: "p1", temp: 33),
        City(name: "City003", image: "p2", temp: 33),
        City(name: "City004", image: "p3", temp: 33),
        City(name: "City005", image: "p4", temp: 33),
        City(name: "City006", image: "p5", temp: 33),
        City(name: "City007", image: "p6", temp: 33)
    ]

    override func setUp() {
        super.setUp()
        sut = SurfingSpotsViewModel()
    }
    
    func testGetRandomTemperature() {
        
        // check that temperature range is between allowed range
        let allowedRange = Constants.MIN_TEMP...Constants.MAX_TEMP
        let test = sut.getRandomTemperature()
        XCTAssert(allowedRange.contains(test), "Temperature range should fall between allowed range")
    }
    
    func testUpdateTemperature() {

        // check that when temperature update occurs and
        // temp = 3 has occurred less that 3 times
        // only one city is updated

        sut.cities = cleanCityList

        sut.updateTemperature(temp: "10")
        let testOne = sut.cities.filter {
            $0.temp == 10
        }.count

        XCTAssert(testOne == 1, "One city must be updated")

        // check that when temperature update occurs and
        // temp = 3 has occurred 3 times
        // three cities are updated

        sut.cities = cleanCityList

        sut.occurrencesOfNumberThree = 2

        sut.updateTemperature(temp: "3")
        let testTwo = sut.cities.filter {
            $0.temp == 3
        }.count

        XCTAssert(testTwo == 3, "Three cities must be updated")

    }
    
    func testGetRandomIndicesFromArray() {
        
        // check that when random indices from array are requested
        // the number of items returned matches with
        // the actual request
        
        sut.cities = cleanCityList
        
        let howManyElements = 3
        
        let testOne = sut.getRandomIndicesFromArray(array: sut.cities, numberOfElements: howManyElements)
        
        XCTAssert(testOne.count == howManyElements, "Returned elements count should match request")
        
        // also check that no duplicates occur
        // so the func works correctly
        // 100 iterations are performed
        
        for _ in 1...100 {
            let testTwo = Set(testOne.map { $0 })
            XCTAssert(testTwo.count == howManyElements, "Returned elements should be unique")
        }
        
        
    }
    
    
    func testGetCitiesNamesForSuccess() {

        guard let url = URL(string: Constants.CITIES_URL) else { return }
        let req = URLRequest(url: url)
        self.mock = URLSessionMock()
        let expectation = self.expectation(description: "getCities")


        self.mock.dataTaskPublisher(for: req)
        .map { $0.data }
        .decode(type: Cities.self, decoder: JSONDecoder())
        .replaceError(with: Cities(cities: []))
        .eraseToAnyPublisher()
        .sink(receiveCompletion: { completion in
            print(completion)
        }, receiveValue: { cities in

            self.sut.cities = cities.cities.enumerated().map {
                City(name: $0.element.name, image: "p\($0.offset)", temp: self.sut.getRandomTemperature())
            }.sorted {
                guard let t0 = $0.temp, let t1 = $1.temp else {
                    return false
                }
                return t0 > t1
            }

            expectation.fulfill()

        })
        .store(in: &subscriptions)

        waitForExpectations(timeout: 5, handler: nil)
        XCTAssert(sut.cities.count == cleanCityList.count, "Cities count should match")

    }
    
    
    func testGetCitiesNamesForFailure() {

        guard let url = URL(string: Constants.MALFORMED_RESPONSE_URL) else { return }
        let req = URLRequest(url: url)
        self.mock = URLSessionMock()
        let expectation = self.expectation(description: "getCitiesMalformedResponse")

        self.mock.dataTaskPublisher(for: req)
        .map { $0.data }
        .decode(type: Cities.self, decoder: JSONDecoder())
        .replaceError(with: Cities(cities: []))
        .eraseToAnyPublisher()
        .sink(receiveCompletion: { completion in
            print(completion)
        }, receiveValue: { value in

            print(value)

            self.sut.cities = value.cities.enumerated().map {
                City(name: $0.element.name, image: "p\($0.offset)", temp: self.sut.getRandomTemperature())
            }.sorted {
                guard let t0 = $0.temp, let t1 = $1.temp else {
                    return false
                }
                return t0 > t1
            }

            expectation.fulfill()

        })
        .store(in: &subscriptions)

        waitForExpectations(timeout: 5, handler: nil)
        XCTAssert(sut.cities.count == 0, "Malformed response should return an empty list")
    }

    
    func testGetNewTemperature() {
        
        guard let url = URL(string: Constants.TEMPERATURE_URL.replacingOccurrences(of: "%min", with: String(Constants.MIN_TEMP)).replacingOccurrences(of: "%max", with: String(Constants.MAX_TEMP))) else { return }
        let req = URLRequest(url: url)
        self.mock = URLSessionMock()
        let expectation = self.expectation(description: "getNewTemperature")
        var testTemperature: Int = 0
        
        self.mock.dataTaskPublisher(for: req)
        .map { (String(data: $0.data, encoding: .utf8)?.split(separator: " ")[0] ?? "0") }
        .replaceError(with: "")
        .sink(receiveValue: { temperature in
            testTemperature = Int(String(temperature)) ?? 0
            expectation.fulfill()
        })
        .store(in: &subscriptions)
        
        waitForExpectations(timeout: 5, handler: nil)
        
        let allowedRange = Constants.MIN_TEMP...Constants.MAX_TEMP
        XCTAssert(allowedRange.contains(testTemperature), "Temperature range should fall between allowed range")
    }
    
    
    func testGetNewTemperatureForFailure() {
        
        guard let url = URL(string: Constants.MALFORMED_RESPONSE_URL) else { return }
        let req = URLRequest(url: url)
        self.mock = URLSessionMock()
        let expectation = self.expectation(description: "getNewTemperatureFail")
        var testTemperature: Int = 0
        
        self.mock.dataTaskPublisher(for: req)
        .map { (String(data: $0.data, encoding: .utf8)?.split(separator: " ")[0] ?? "0") }
        .replaceError(with: "")
        .sink(receiveValue: { temperature in
            testTemperature = Int(String(temperature)) ?? 0
            expectation.fulfill()
        })
        .store(in: &subscriptions)
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssert(testTemperature == 0, "Temperature should be 0")
    }
    
}

// url session mock

protocol APIDataTaskPublisher {
    func dataTaskPublisher(for request: URLRequest) -> URLSession.DataTaskPublisher
}

class URLSessionMock: APIDataTaskPublisher {
    
    func dataTaskPublisher(for request: URLRequest) -> URLSession.DataTaskPublisher {
        let stubReply = request.url?.lastPathComponent ?? "stub_error"
        let req = Bundle(for: type(of: self)).url(forResource: stubReply, withExtension: "json")!
        return URLSession.shared.dataTaskPublisher(for: req)
    }
}
