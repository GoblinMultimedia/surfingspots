//
//  SurfingSpotViewModel.swift
//  SurfingSpots
//
//  Created by Paolo Piccini on 29/07/2020.
//  Copyright © 2020 Paolo Piccini. All rights reserved.
//

import Foundation
import Combine

class SurfingSpotsViewModel: ObservableObject {
    
    var occurrencesOfNumberThree = 0 // if this counter amounts to 3, three cities change temperature at once
    @Published var cities: [City] = [] // contains the full city data and as @Published var allows for reactive response
    let networkLayer = NetworkLayer()
    
    
    
    func getCitiesNames() {
        
        // called just once, retrieves the basic city info from the mocky URL
        // and uses it to create EnhancedCity structs that include the full city data
        // adding id, temperature and image reference
        
        networkLayer.getData(from: Constants.CITIES_URL, modelType: Cities.self, onReceiveAction: { cities in
            DispatchQueue.main.async {
                self.cities = cities.cities.enumerated().map {
                    City(name: $0.element.name, image: "p\($0.offset)", temp: self.getRandomTemperature())
                }.sorted {
                    guard let t0 = $0.temp, let t1 = $1.temp else {
                        return false
                    }
                    return t0 > t1
                }
            }
        })
        
    }
    
    
    func getNewTemperature() {
        
        // retrieves temperature data from url
        
        networkLayer.getData(from: Constants.TEMPERATURE_URL, modelType: Temperature.self, onReceiveAction: { temperature in
            DispatchQueue.main.async {
                self.updateTemperature(temp: String(temperature.number))
            }
        })
        
    }
    
    
    func updateTemperature(temp: String) {
        
        // once temperature info is retrieved, actual city temperature can be updated
        //
        // occurrences of temperature = 3 are taken in account as per app specification
        // if occurrences are less than 3, one city temperature is updated
        // choosing a random index from the citiez array and assigning to the relevant city
        // the temperature retrieved in the previous step
        //
        // if occurrences == 3 a function retrieves 3 random indices from the citiez array
        // and three cities are updated at once - also occurrences are reset to 0, ready to fire again in the future
        
        
        // if cities are not yet loaded
        if cities.count == 0 {
            return
        }
        
        if (temp == "3") {
            self.occurrencesOfNumberThree += 1
        }
        
        if occurrencesOfNumberThree == 3 {
            
            let randomCitiesIndices = self.getRandomIndicesFromArray(array: cities, numberOfElements: 3)
            for i in randomCitiesIndices {
                cities[i].temp = Int(temp) ?? 0
            }
            
            occurrencesOfNumberThree = 0
            
        } else {
            
            let randomIndex = Int.random(in: 0..<cities.count)
            cities[randomIndex].temp = Int(temp) ?? 0
            
        }
        
        DispatchQueue.main.async {
            self.cities = self.cities.sorted {
                guard let t0 = $0.temp, let t1 = $1.temp else {
                    return false
                }
                return t0 > t1
            }
        }
    }
    
    
    func getRandomTemperature() -> Int {
        
        // used only on first init of the citiez object
        // chooses a random temperature in the allowed range and assigns it
        // to the EnhancedCity struct on init
        
        Int.random(in: Constants.MIN_TEMP...Constants.MAX_TEMP)
    }
    
    func getRandomIndicesFromArray<T>(array: [T], numberOfElements: Int) -> [Int] {
        
        // returns an [Int] containing the number of random indices from the citiez array specified in the numberOfElements parameter
        // the first parameter is the actual array from which the random indices should be extracted
        // the paramter is a generic even though in this context is used only with one type, but it's still good practice :-)
        //
        // a temp Array is inited with a number stride that equals to the count of the passed array
        // then a shuffle operation is performed on the temp array to ensure randomness
        // finally, the temp array is enumerated, filtered and mapped to return the random indices
        // that will be used to update multiple cities at once
        
        let temp = Array(stride(from: 0, to: array.count - 1, by: 1))
        let indices = temp.shuffled().enumerated().filter {
            $0.offset < numberOfElements
        }.map {
            $0.element
        }
        return indices
    }
    
    
}



