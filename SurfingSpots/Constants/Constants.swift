//
//  Constants.swift
//  SurfingSpots
//
//  Created by Paolo Piccini on 29/07/2020.
//  Copyright © 2020 Paolo Piccini. All rights reserved.
//

import Foundation

class Constants {
    
    static let CITIES_URL = "https://run.mocky.io/v3/652ceb94-b24e-432b-b6c5-8a54bc1226b6"
    static let TEMPERATURE_URL = "http://numbersapi.com/random?min=1&max=40&json"
    static let TEMPERATURE_REQUEST_INTERVAL = TimeInterval(3.0)
    static let MIN_TEMP = 0
    static let MAX_TEMP = 40
    
    // test url
    static let MALFORMED_RESPONSE_URL = "https://www.test.it/malformedResponse"
    
    
}
