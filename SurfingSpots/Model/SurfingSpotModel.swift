//
//  SurfingSpotModel.swift
//  SurfingSpots
//
//  Created by Paolo Piccini on 29/07/2020.
//  Copyright © 2020 Paolo Piccini. All rights reserved.
//

import Foundation

// MARK: - Cities
struct Cities: Codable {
    let cities: [City]
}

struct City: Codable, Identifiable {
    let id = UUID()
    let name: String
    let image: String?
    var temp: Int?
}

struct Temperature: Codable {
    let number: Int
}


