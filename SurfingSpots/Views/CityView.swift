//
//  CityView.swift
//  SurfingSpots
//
//  Created by Paolo Piccini on 30/07/2020.
//  Copyright © 2020 Paolo Piccini. All rights reserved.
//

import SwiftUI

struct CityView: View {
    
    var cityInfo: City
    
    var isSunny : Bool {
        cityInfo.temp ?? 0 >= 30
    }
    
    var body: some View {
        
        ZStack (alignment: .bottomLeading) {
            
            getImageView()
             
            VStack (alignment: .leading) {
                Text(cityInfo.name)
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .padding(.bottom, 5)
                Text("\(getWeatherStatus()) - \(getDegrees())")
                    .font(.subheadline)
                    .fontWeight(.bold)
            }
            .shadow(radius: 5)
            .padding()
            
        }
        .foregroundColor(.white)
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 250)
        
    }
    
    
    // utility functions
    
    
    // returns the assigned image
    // or a gray background
    // based on temperature range
    
    func getImageView() -> AnyView {
        if isSunny {
            return AnyView(Image(cityInfo.image ?? "placeholder")
            .resizable()
            .aspectRatio(1.63, contentMode: .fit)
            .cornerRadius(20))
        } else {
            return AnyView(Color.gray
            .aspectRatio(1.63, contentMode: .fit)
            .cornerRadius(20))
        }
    }
    
    
    // returns the current weather status as a string
    func getWeatherStatus() -> String {
        isSunny ? "Sunny" : "Cloudy"
    }
    
    // returns the current temperature in degrees
    // accounts for errors, i.e. if something goes wrong
    // with the API call, temperature is set to a
    // conventional 0 (out of the allowed range)
    // and a "not available" message is displayed
    func getDegrees() -> String {
        if (cityInfo.temp == 0) {
            return ("degrees: n/a")
        } else {
            return "\(cityInfo.temp ?? 0) degrees"
        }
    }
    
    
}

struct CityView_Previews: PreviewProvider {
    static var previews: some View {
        CityView(cityInfo: City(name: "Boston", image: "p4", temp: 32))
    }
}
