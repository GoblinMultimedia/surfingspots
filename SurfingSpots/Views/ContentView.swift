//
//  ContentView.swift
//  SurfingSpots
//
//  Created by Paolo Piccini on 29/07/2020.
//  Copyright © 2020 Paolo Piccini. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject private var viewModel = SurfingSpotsViewModel()
    let timer = Timer.publish(every: Constants.TEMPERATURE_REQUEST_INTERVAL, on: .main, in: .common).autoconnect()
    // temperature request is fired every 3 seconds
    
    init() {
        UITableView.appearance().separatorStyle = .none
        self.viewModel.getCitiesNames()
        
    }
    
    var body: some View {
        
        VStack (spacing: 0) {
           
            VStack {
                Text("Surfing Spots")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .padding(.top, 70)
                        .padding(.bottom, 30)
                        .padding(.horizontal, 30)
            }
            .frame(minWidth:0, maxWidth: .infinity, alignment: .leading)
            .background(Color.init(.systemGray6))
            .edgesIgnoringSafeArea(.all)
            
            
            List(viewModel.cities) { city in
                CityView(cityInfo: city).padding(.bottom)
            }
                .edgesIgnoringSafeArea(.all)
            .onReceive(timer) { _ in
                self.viewModel.getNewTemperature() 
            }
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
