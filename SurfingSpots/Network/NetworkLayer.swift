//
//  NetworkLayer.swift
//  SurfingSpots
//
//  Created by Paolo Piccini on 06/08/2020.
//  Copyright © 2020 Paolo Piccini. All rights reserved.
//

import Foundation
import Combine

class NetworkLayer {
    
    var cancellable: AnyCancellable?
    
    func getData<T>(from url: String, modelType: T.Type, onReceiveAction: @escaping (T)->()) where T: Codable {
        
        guard let myURL = URL(string: url) else { return }
        
        let remoteDataPublisher = URLSession.shared.dataTaskPublisher(for: myURL)
            // dataTaskPublisher returns (data: Data, response: URLResponse)
            .map { $0.data }
            .decode(type: T.self, decoder: JSONDecoder())
        
        cancellable = remoteDataPublisher
            .sink(receiveCompletion: { completion in
                print("Completion:", String(describing: completion))
                switch completion {
                case .finished:
                    break
                case .failure(let theError):
                    print("received error: ", theError)
                }
            }, receiveValue: { someValue in
                onReceiveAction(someValue)
                
            })
    }
    
}
